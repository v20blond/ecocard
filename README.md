# EcoCard

Ce projet permet d'éteindre et d'allumer un ordinateur grâce à une carte.
Tous les fichiers du git doivent être placés sur le Bureau (Desktop) de la Raspberry ou bien il faut modifier le chemin d'accès aux fichiers dans le code lui-même.

**Installations** :
  - Il faut en amont télécharger Raspbian sur votre carte SD : https://raspberry-pi.fr/creez-carte-sd-raspbian-raspberry-pi-windows/
  - Il faut installer VNC si vous souhaitez commander l'interface grapghique à distance
    1. Télécharger VNC viewer sur ton ordinateur : https://www.realvnc.com/fr/connect/download/viewer/windows/
    2. Téléchargez le fichier conf dans le git. Changer le nom et le mot de passe selon le réseau wifi que tu utilises sur ton pc. Enregistre le en .conf
    3. place le fichier .conf dans le boot de la carte sd. Ensuite, enlever la carte sd et la remettre la dans la raspberry pi éteinte. Cela permet de configurer le réseau sur lequel se connecte la raspberry pi de base.
    4. Ouvre VNC viewer et rentre 172.20.10.9 (adresse IP de la Raspberry) dans l’adresse VNC server. Cela va ouvrir un onglet avec id et mdp. l’id est : Pi et le mot de passe est : raspberry.

**Librairies** :

Plusieurs librairies sont nécéssaires pour l'execution du code et nécessitent d'être installées.
Il Faut taper les commandes suivantes:
 - `sudo apt-get install samba-common` pour le Shutdown
 - `sudo apt-get install etherwake` pour le Wake on Lan
 - `sudo apt-get install winexe` pour la mise en veille
 - `sudo apt-get install subprocess` pour executer des fichiers script

Il est possible que ces librairies doivent être téléchargées en amont.*

**Configurer Lancement.sh**

Il faut configurer lancement.sh pour que ce script s'execute dès le démarrage de la carte.
- Taper la commande `sudo crontab -e`
- Appuyer sur Enter si c'est la première fois.
- A la fin du crontab taper : `@reboot sh /home/pi/Desktop/lancement.sh`
- Appuyer sur Ctrl+O puis Enter et quitter la page

**Wake On Lan** :

Pour le Wake on Lan il faut vérifier sur la notice d'utilisation de l'ordinateur que celui-ci est compatible au Wake On Lan.
Il Faut également :
 - activer l'option Wake On Lan dans le BIOS. Cette option se situe souvent dans l'onglet "power management". Cette option peut également s'appeler "Power on by PCI-E/PCI" ou "Power on from S5 by PME".
 - Il faut ensuite se rendre dans les paramètres de la carte réseau > Cliquer sur la carte Ethernet utilisée.
   Dans l'onglet avancé > Activer l'option Wake on magic Packet.
   Dans l'onglet Gestion de l'alimentation, tous les éléments doivent être cochés, c'est à dire :
	- "Autoriser ce périphérique à sortir l'ordinateur du mode veille"
	- "Autoriser uniquement un paquet magique à sortir l'ordinateur du mode veille"
Il faut également récupérer l'adresse MAC de l'ordinateur à allumer via la commande ipconfig /all

**ShutDown**

Il faut tout d'abord vérifier que le port TCP 445 de l'ordinateur est ouvert.
 - Bouton Windows > Panneau de configuration > Système et sécurité > Autorisé une application via le Pare-feu Windows.
   Descendre ensuite jusqu'à “Partage de fichiers et d’imprimantes” et vérifier que la case est bien cochée.
 - Revenir au Panneau de configuration et tapez distance dans la barre de recherche > “Autoriser l’accès à distance à votre ordinateur”.
   Cocher “Autoriser les connexions à distance à cet ordinateur”

Il faut récupérer l'ip ethernet de l'ordinateur à éteindre via la commande ipconfig ainsi que l'ID et le mot de passe d'une session administrateur.

**Quelques erreurs fréquentes** :
 - Il peut être nécessaire d'effectuer une mise à jour du système : `sudo apt-get update` et `sudo apt-get upgrade` 
 - E : Could not initialise pipe winreg. Error was NT_STATUS_OBJECT_NAME_NOT_FOUND
   Ouvrir services.msc et mettre le service "Registre à distance" sur manuel.
 - E : WERR_CALL_NOT_IMPLEMENTED
   Ouvrir le registre regedit.exe et créer un clé sous "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System"
   cela doit être une clé 32-bit DWORD qui est appelée LocalAccountTokenFilterPolicy. Fixer la valeur de cette clé sur 1.
 - Si il y un Accès refusé lors du shutdown. Dans démarrer, taper gpedit.msc (il peut être installé manuellement sur les versions Windows Home).
   "Configuration Ordinateur" > "Paramètres Windows">"Paramètres de Sécurité">"Stratégies locales">"attribution des droits utilisateurs">"Forcer l'arrêt à partir d'un système distant"
   Vous double clickez dessus et là vous tapez "Tout le monde" dans la liste des utilisateurs habilités à faire un shutdown à distance. 
