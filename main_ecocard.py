#!/usr/bin/env python3.5
#-- coding: utf-8 --

import RPi.GPIO as GPIO #Importe la bibliothèque pour contrôler les GPIOs
from pirc522 import RFID
import time
import subprocess #Importe la bibliothèque qui permet de faire appel aux scripts


GPIO.setmode(GPIO.BOARD) #Définit le mode de numérotation (Board)
GPIO.setwarnings(False) #On désactive les messages d'alerte

RFID_UID = [208, 64, 7, 37, 178] #Définit l'UID du badge RFID


rc522 = RFID() #On instancie la lib

print('En attente d un badge (pour quitter, Ctrl + c): ') #On affiche un message demandant à l'utilisateur de passer son badge

#On va faire une boucle infinie pour lire en boucle
while True :
    rc522.wait_for_tag() #On attnd qu'une puce RFID passe à portée
    (error, tag_type) = rc522.request() #Quand une puce a été lue, on récupère ses infos

    if not error : #Si on a pas d'erreur
        (error, uid) = rc522.anticoll() #On nettoie les possibles collisions, ça arrive si plusieurs cartes passent en même temps

        if not error : #Si on a réussi à nettoyer
            if RFID_UID == uid :
                print('Badge {} autorisé !'.format(uid))
                subprocess.call("/home/pi/Desktop/shutdown.sh") #On appel le script shutdown qui permet de shutdown un ordinateur à distance
                #subprocess.call("/home/pi/Desktop/wake_on_lan.sh") #On appel le script wake_on_lan qui permet d'envoyer un paquet magique à l'ordinateur à allumer
            else:
                print('Badge {} interdit !'.format(uid))

            time.sleep(1) #On attend 1 seconde pour ne pas lire le tag des centaines de fois en quelques milli-secondes

