#Ce script permet d'éteindre un ordinateur à distance
#169.254.55.180 est l'adresse Ip Ethernet de l'ordinateur à éteindre
#login doit être le nom d'utilisateur d'une session administrateur de l'ordinateur à éteindre
#motdepasse est le mot de passe correspondant au nom d'utilisateur login
#-t définit le temps d'attente avant d'initier cette commande
#-f Force les applications en cours d'exécution à se fermer sans avertissement.

#!/bin/bash
net rpc shutdown -f -I 169.254.55.180 -U login%motdepasse -t 1 -C "Arrêt en cours..."
